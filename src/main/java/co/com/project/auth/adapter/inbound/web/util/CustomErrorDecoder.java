package co.com.project.auth.adapter.inbound.web.util;

import co.com.project.auth.domain.exception.UserClientException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class CustomErrorDecoder implements ErrorDecoder {
    @Override
    public Exception decode(String methodKey, Response response) {
        return new UserClientException();
    }
}
