package co.com.project.auth.adapter.inbound.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder(builderMethodName = "init")
public class UserDTO extends EntityBaseDTO {
    private String name;
    private String lastname;
    private String username;
    private String password;
    private String cellphone;
    private String direction;
}
