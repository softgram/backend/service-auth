package co.com.project.auth.adapter.outbound.repository;

import co.com.project.auth.adapter.outbound.client.UserClient;
import co.com.project.auth.adapter.outbound.model.UserDetailsImpl;
import co.com.project.auth.domain.repository.UserDetailRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class UserDetailRepositoryImpl implements UserDetailRepository {
    private final UserClient client;

    @Override
    public UserDetails getUserDetails(String username) {
        return new UserDetailsImpl(client.findByUsername(username).getData());
    }
}
