package co.com.project.auth.adapter.outbound.client;

import co.com.project.auth.configuration.FeignConfiguration;
import co.com.project.auth.adapter.inbound.web.dto.UserDTO;
import co.com.project.common.adapter.inbound.web.dto.Response;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "service-security", path = "/api/users", configuration = FeignConfiguration.class)
public interface UserClient {
    @GetMapping("/username/{username}")
    Response<UserDTO> findByUsername(@PathVariable String username);
}
