package co.com.project.auth.adapter.inbound.web.util;

import co.com.project.auth.domain.constant.TokenConstant;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TokenUtil {

    public static String createToken(String name, String username) {
        Map<String, Object> properties = new HashMap<>();
        properties.put("name", name);

        return Jwts.builder()
                .setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis() +
                        TokenConstant.ACCESS_TOKEN_VALIDITY_SECONDS * 1000))
                .addClaims(properties)
                .signWith(Keys.hmacShaKeyFor(TokenConstant.ACCESS_TOKEN_SECRET.getBytes()))
                .compact();
    }

    public static UsernamePasswordAuthenticationToken getAuthentication(String token) {
        Jws<Claims> claimsJws = Jwts.parserBuilder()
                .setSigningKey(TokenConstant.ACCESS_TOKEN_SECRET.getBytes())
                .build()
                .parseClaimsJws(token);

        String username = claimsJws.getBody().getSubject();

        return new UsernamePasswordAuthenticationToken(username, null, Collections.emptyList());
    }

    public static void validateToken(final String token) {
        Jwts.parserBuilder().setSigningKey(TokenConstant.ACCESS_TOKEN_SECRET.getBytes()).build().parseClaimsJws(token);
    }
}

