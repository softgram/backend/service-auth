package co.com.project.auth.domain.exception;

import co.com.project.common.domain.exception.ApplicationException;
import lombok.experimental.StandardException;

@StandardException
public class UserClientException extends ApplicationException {
    public UserClientException() {
        super("Error in client to connect to service.");
    }
}
