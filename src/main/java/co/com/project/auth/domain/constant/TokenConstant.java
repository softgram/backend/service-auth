package co.com.project.auth.domain.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TokenConstant {
    public static final Long ACCESS_TOKEN_VALIDITY_SECONDS = 2_592_000L;
    public static final String ACCESS_TOKEN_SECRET = "asidodfu8923u4jiuhvbc98bu9q3y8yv";
}
