package co.com.project.auth.domain.repository;

import org.springframework.security.core.userdetails.UserDetails;

public interface UserDetailRepository {
    UserDetails getUserDetails(String username);
}
