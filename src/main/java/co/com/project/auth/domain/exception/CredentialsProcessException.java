package co.com.project.auth.domain.exception;

import co.com.project.common.domain.exception.ApplicationException;
import lombok.experimental.StandardException;

@StandardException
public class CredentialsProcessException extends ApplicationException {
    public CredentialsProcessException() {
        super("Error to process required credentials.");
    }
}
