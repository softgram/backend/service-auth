FROM openjdk:20 as builder

WORKDIR /app

COPY ./build/libs/service-auth-1.0-SNAPSHOT.jar .

ENTRYPOINT java -jar ./service-auth-1.0-SNAPSHOT.jar